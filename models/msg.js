const mongoose = require('mongoose')
const msgSch = new mongoose.Schema(
    {
        msg:{
            type: String,
            required: true
        },      
         username:{
            type:String,
            required:true
        },
        date:{
            type:Date,
            required:true
        }

    }
)

const Msg = mongoose.model('msg',msgSch)
module.exports = Msg