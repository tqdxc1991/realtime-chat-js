const express = require('express')
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
let users = [];
const PORT = 3005
const Msg = require('./models/msg.js')
const mongoose = require('mongoose')

const mongoDB = 'mongodb+srv://qiong:qiong123@cluster0.qyii0.mongodb.net/msg-db?retryWrites=true&w=majority'


// connect to mongoDB
mongoose.connect(mongoDB,{ useNewUrlParser: true,
    useUnifiedTopology: true } ).then(()=>{
      console.log('DB connected')
      }).catch(err=>(console.log(err)));


// On sert le dossier plublic/
app.use(express.static('public'));

// On se sert de socket IO pour emettre / diffuser des événéments
io.on('connection', (socket) => {

  // On garde l'id du user
  const userId = socket.id

  // Ici: renvoyer l'historique stocké dans mongo ?
  Msg.find().then(result =>{
    socket.emit('hMessage',result);
  })
  // Sur l'action d'une nouvelle connextion d'un utilisateur
  socket.on('newUser', (username) => {
    // On le rajoute à la liste des utiliseurs
    users.push({id: userId, username: username});

    // On prévient tout le monde de son arrivé
    io.emit('newNotification', username + ' vient de se connecter');

    // On demande à tous le monde mettre à jour la liste des utilisateurs
    io.emit('updateUsersList', users);
  });

  // Sur l'action d'un nouveau message reçu
  socket.on('newMessage', (username, msg, date) => {
    
    // Ici: stocker dans mongo ? 
    const message =  new Msg({username, msg, date});
    message.save().then(()=>{
          // On diffuse le message à tout le monde
         io.emit('newMessage', username, msg, date);
    })

  });





    socket.on('newP', newPdo => {
      // On garde l'id du user
      const userId = socket.id
     // find the user with the same userID and change his username
      for(const user of users){
        if(user.id == userId){
        user.username = newPdo;
        //On demande à tous le monde mettre à jour la liste des utilisateurs
        io.emit('updateUsersList', users);
        socket.emit('validPdo',newPdo);
        }
      }
    });

  // Sur l'action d'une déconnextion d'un utilisateur
  socket.on('disconnect', (username) => {



    // On cherche le user dans la liste pour le retirer
    users = users.filter(user => {
      return user.id != userId;
    });

    // On demande à tous le monde mettre à jour la liste des utilisateurs
    // io.emit('updateUsersList', users);
    io.emit('deleteUsersList', users);

    // On prévient tout le monde de son arrivé
    io.emit('uLeave', username);
  });
});

http.listen(PORT, () => {
  console.log('listening on http://localhost:' + PORT);
});

