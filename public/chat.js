const socket = io()
let username = localStorage.getItem('pseudo')
const messages = document.getElementById('messages')
const form = document.getElementById('form')
const input = document.getElementById('input')

// Demande du pseudo utilisateur (si pas dans le localstorage)
while (!username) {
    username = prompt('Quel est votre pseudo')
    localStorage.setItem('pseudo', username)
}
console.log('local storage',localStorage);

// Envoi du nouveau user
socket.emit('newUser', username)

// Envoi d'un message
form.addEventListener('submit', function(e) {
    e.preventDefault()
    if (input.value) {
        const msg = input.value;
        const date = new Date();
        socket.emit('newMessage', username, msg, date)
        input.value = ''
    }
})

// Récupération des nouveaux messages
socket.on('newMessage', function(username, msg, date) {
    var item = document.createElement('li')
    item.textContent = username + ': ' + msg + ' - ' + date
    messages.appendChild(item)
    window.scrollTo(0, document.body.scrollHeight)
})

// Récupération des nouvelles notification
socket.on('newNotification', function(msg) {
    var item = document.createElement('li')
    item.classList = 'notif'
    item.textContent = msg
    messages.appendChild(item)
    window.scrollTo(0, document.body.scrollHeight)
})

// Envoi du nouveau user
socket.emit('uLeave', username);

// user left notification
socket.on('uLeave', function(username) {
    var item = document.createElement('li')
    item.classList = 'notif'
    item.textContent = username + ' vient de se déconnecter'
    messages.appendChild(item)
    window.scrollTo(0, document.body.scrollHeight)
})

// Récupération de la liste des utilisateurs
socket.on('updateUsersList', function(users) {
    // Mettre à jour la liste dans le DOM (document) => Page html
    console.log('update Users list : ',users );
    const userList = document.getElementById('users');
    //remettre la lise vide
    userList.innerHTML ='';
    
    // mettre à jour la liste user
    for(i = 0; i < users.length; i ++){
        // console.log(users[i].username);
        const username = users[i].username;
        const uId = users[i].id;
        const li = document.createElement("li");
        li.innerHTML = username;
        li.id = uId;
        console.log('logging each user id',uId);
        userList.appendChild(li);
    }
})

//update userlist when someone leaves
socket.on('deleteUsersList', function(users) {
    // Mettre à jour la liste dans le DOM (document) => Page html
    console.log('someone left, now update Users list : ',users );
    const userList = document.getElementById('users');
    userList.innerHTML ='';
    // mettre à jour la liste user
    for(i = 0; i < users.length; i ++){
        // console.log(users[i].username);
        const username = users[i].username;

        const li = document.createElement("li");
        li.innerHTML = username;

        userList.appendChild(li);
    }
})

// load history messages
socket.on('hMessage', data => {
    console.log('history',data);
    if(data.length){
        data.forEach(message=>{
            var item = document.createElement('li')
            item.textContent = message.username + ': ' + message.msg + ' - message envoyé à ' +  message.date 
            messages.appendChild(item)
            window.scrollTo(0, document.body.scrollHeight)
        })
    }
    
}

    
    )

    //when we click the button changer pseuso, send to server
    document.getElementById('changeP').addEventListener('click',function(e){
        let newPdo = prompt('ecrire votre la nouvelle pseudo')
       
        // Envoi du nouveau pseudo
        if(newPdo!=null){
            socket.emit('newP', newPdo)
        }

        socket.on('validPdo',newPdo=>{
            localStorage.setItem('pseudo', newPdo)
            console.log("pseudo changed successfully",localStorage)
        });
    })

